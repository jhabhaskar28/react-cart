import axios from "axios";
import React from "react";
import {
    Link
} from "react-router-dom";
import '../CartProduct.css';

class CartProduct extends React.Component{

    constructor() {
        super();

        this.API_STATES = {
            LOADED: 'loaded',
            LOADING: 'loading',
            ERROR: 'error'
        };

        this.state = {
            product: null,
            status: this.API_STATES.LOADING,
            errorMessage: ''
        };
    }

    componentDidMount(){

        axios.get(`https://fakestoreapi.com/products/${this.props.productId}`)
        .then((response) => {
            this.setState({
                product: response.data,
                status: this.API_STATES.LOADED
            });
        })
        .catch((err) => {
            console.error(err);

            this.setState({
                status: this.API_STATES.ERROR,
                errorMessage: 'API error occurred. Please try again!'
            });
        });
    }

    render(){

        return (

            <>

                {this.state.status === this.API_STATES.ERROR &&
                    <div className='failedAPI'>
                        <h1>{this.state.errorMessage}</h1>
                    </div>
                }

                {this.state.status === this.API_STATES.LOADING &&
                    <div className="loaderLogo">
                        <div className="lds-facebook"><div></div><div></div><div></div></div>
                    </div>
                }

                {this.state.status === this.API_STATES.LOADED && 
                    <Link to={`/${this.props.productId}`}>
                        <div className='cartProductItem'>
                            <div className='imageProduct'>
                                <img src={this.state.product.image} alt="Item" />
                            </div>
                            <div className='descriptionProduct '>
                                <h2 className="productTitle">{this.state.product.title}</h2>
                                <h3>Category: {this.state.product.category}</h3>
                                <h2>Quantity: {this.props.quantity}</h2> 
                                <h2 className="productPrice">Price: ${this.state.product.price}</h2>
                            </div>
                        </div>
                    </Link>
                }
            </>

        );
    }
}

export default CartProduct;