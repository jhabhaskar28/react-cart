import axios from 'axios';
import React from 'react';
import { Link } from 'react-router-dom';
import CartItem from './CartItem';
import '../Cart.css';

class Cart extends React.Component{

    constructor(){
        super();

        this.API_STATES = {
            LOADED: 'loaded',
            LOADING: 'loading',
            ERROR: 'error'
        };

        this.state = {
            cartItems: null,
            status: this.API_STATES.LOADING,
            errorMessage: ''
        };
        
    }

    componentDidMount(){
        
        axios.get('https://fakestoreapi.com/carts')
        .then((response) => {
            this.setState({
                cartItems: response.data,
                status: this.API_STATES.LOADED
            });
        })
        .catch((err) => {
            console.error(err);

            this.setState({
                status: this.API_STATES.ERROR,
                errorMessage: "API error occurred. Please try again!"
            });
        });
    }

    render(){
        
        return (
            <>
            
            {this.state.status === this.API_STATES.ERROR &&
                <div className='failedAPI'>
                   <h1>{this.state.errorMessage}</h1>
                </div>
            }

            {this.state.status === this.API_STATES.LOADING && 
                <div className="loaderLogo">
                    <div className="lds-facebook"><div></div><div></div><div></div></div>
                </div>
            }

            {this.state.status === this.API_STATES.LOADED && this.state.cartItems.length === 0 && 
                <h1 className="noProducts">Sorry! No products to display.</h1>
            }    
            
            {this.state.status === this.API_STATES.LOADED && this.state.cartItems.length > 0 && 
                    <div className='cartsContainer'>
                        <header>
                            <h1 className='headerText'>THE FAKE STORE</h1>
                            <Link to='/'><button>Home</button></Link>
                        </header>
                        <div className="cartItemContainer">
                            {this.state.cartItems.map((cartItem) => {
                                return <CartItem key={`${cartItem.id}${cartItem.userId}`} id={cartItem.id} userId={cartItem.userId} products={cartItem.products} date={cartItem.date}/>
                            })}
                        </div>
                    </div>
            }
            </>
        );
    }
}

export default Cart;