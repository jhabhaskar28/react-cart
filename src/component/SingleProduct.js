import React from 'react';
import '../Product.css';
import '../SingleProduct.css';
import axios from 'axios';
import { Link } from 'react-router-dom';

class SingleProduct extends React.Component{
    
    constructor(){
        super();

        this.API_STATES = {
            LOADING: 'loading',
            LOADED: 'loaded',
            ERROR: 'error'
        }
        this.state = {
          item: null,
          status: this.API_STATES.LOADING,
          errorMessage: ''
        };
    }

    componentDidMount(){

        axios.get(`https://fakestoreapi.com/products/${this.props.match.params.id}`)
        .then((response) => {
          this.setState({
            item: response.data,
            status: this.API_STATES.LOADED
          });
        })
        .catch((err) => {
          this.setState({
            status: this.API_STATES.ERROR,
            errorMessage: 'API error occurred. Please try again!'
          });
        });
    }

    render() {

        return (
            <>
                {this.state.status === this.API_STATES.ERROR && 
                    <div className='failedAPI'>
                        <h1>{this.state.errorMessage}</h1>
                    </div>
                }

                {this.state.status === this.API_STATES.LOADING && 
                    <div className="loaderLogo">
                        <div className="lds-facebook"><div></div><div></div><div></div></div>
                    </div>
                }

                {this.state.status === this.API_STATES.LOADED && Object.keys(this.state.item).length === 0 && 
                    <h1 className='noItemPresent'>Sorry! No item to display.</h1>
                }

                { this.state.status === this.API_STATES.LOADED && Object.keys(this.state.item).length > 0 && 
                        <div className='singleProductContainer'>
                            <header>
                                <h1 className='headerText'>THE FAKE STORE</h1>
                                <Link to='/'><button>Home</button></Link>
                            </header>
                            <div className='itemContainer'>
                                <div className='item singleItem'>
                                    <div className='image singleItemImage'>
                                        <img src={this.state.item.image} alt="Item"/>
                                    </div>
                                    <div className='description descriptionSingleProduct'>
                                        <h3>{this.state.item.title}</h3> 
                                        <h4>Category: {this.state.item.category}</h4>
                                        <p>{this.state.item.description}</p>
                                    </div>
                                    <div className='details'>
                                        <h4>Rating: {this.state.item.rating.rate} ({this.state.item.rating.count})</h4>
                                        <h3>Price: ${this.state.item.price}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                }
            </>

        );
    }
}

export default SingleProduct;
