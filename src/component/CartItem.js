import React from "react";
import CartProduct from "./CartProduct";
import '../CartItem.css';

class CartItem extends React.Component{

    render(){
        
        return(
            <div className="cartItem">
                <div className="cartUserHeader">
                    <h2>USER: {this.props.userId}</h2>
                    <h2>DATE: {`${this.props.date.substring(8,10)}/${this.props.date.substring(5,7)}/${this.props.date.substring(0,4)}`}</h2>
                </div>
                <div className="cartProduct">
                    {this.props.products.map((product) => {
                        return <CartProduct key={`${this.props.id}${product.productId}`} quantity={product.quantity} productId={product.productId}/>
                    })}
                </div>
            </div>
        );
    }
}

export default CartItem;