import React from "react";

class ErrorScreen extends React.Component{
    
    render(){
        return(
            <h1 className="noItemPresent">Sorry! No item to display</h1>
        );
    }
}

export default ErrorScreen;
