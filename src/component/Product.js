import React from 'react';
import '../Product.css';
import {
    Link
} from "react-router-dom";

class Product extends React.Component{

    render() {

        return (
                <Link to={`/${this.props.productID}`}>
                    <div className='item' onClick={this.props.func}>
                        <div className='image'>
                            <img src={this.props.image} alt="Item"/>
                        </div>
                        <div className='description'>
                            <h3>{this.props.title}</h3> 
                            <h4>Category: {this.props.category}</h4>
                            <p>{this.props.description}</p>
                        </div>
                        <div className='details'>
                            <h4>Rating: {this.props.rating} ({this.props.count})</h4>
                            <h3>Price: ${this.props.price}</h3>
                        </div>
                    </div>
                </Link>
        );
    }
}

export default Product;
