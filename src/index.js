import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";
import reportWebVitals from './reportWebVitals';
import SingleProduct from './component/SingleProduct';
import ErrorScreen from './component/ErrorScreen';
import Cart from './component/Cart';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
      <Switch>
        <Route path='/' exact>
          <App />
        </Route>
        <Route path='/cart' exact>
          <Cart />
        </Route>
        <Route path='/:id' component={SingleProduct} exact/>
        <Route path='/*'>
          <ErrorScreen />
        </Route>
      </Switch>
    </Router>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
