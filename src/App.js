import React from 'react';
import './App.css';
import Product from './component/Product';
import axios from 'axios';
import { Link } from 'react-router-dom';

class App extends React.Component{

  constructor(){
    super();

    this.API_STATES = {
      LOADING: 'loading',
      LOADED: 'loaded',
      ERROR: 'error'
    };

    this.state = {
      items: null,
      status: this.API_STATES.LOADING,
      errorMessage: ''
    };
  }

  componentDidMount(){
    axios.get('https://fakestoreapi.com/products')
    .then((response) => {
      this.setState({
        items: response.data,
        status: this.API_STATES.LOADED
      });
    })
    .catch((err) => {
      this.setState({
        status: this.API_STATES.ERROR,
        errorMessage: "API error occurred. Please try again!"
      });
    });
  }
 
  render() {

    return(

        <>

          {this.state.status === this.API_STATES.ERROR && 
            <div className='failedAPI'>
              <h1>{this.state.errorMessage}</h1>
            </div>
          }
          
          {this.state.status === this.API_STATES.LOADING &&
            <div className="loaderLogo">
              <div className="lds-facebook"><div></div><div></div><div></div></div>
            </div>
          } 

          {this.state.status === this.API_STATES.LOADED && this.state.items.length === 0 &&
            <h1 className="noProducts">Sorry! No products to display.</h1>
          }

          {this.state.status === this.API_STATES.LOADED && this.state.items.length > 0 && 
            <div className="cartContainer">
              <header>
                <h1 className='headerText'>THE FAKE STORE</h1>
                <Link to='/cart'><button>Cart</button></Link>
              </header>
              <div className="itemContainer">
                {this.state.items.map((item) => {
                    return (
                      <Product  key={item.id} productID={item.id} description={item.description} title={item.title} rating={item.rating.rate} count={item.rating.count} price={item.price} category={item.category} image={item.image}/>
                    )
                })}
              </div>
            </div>
          }

        </>
    );
  }
}



export default App;
